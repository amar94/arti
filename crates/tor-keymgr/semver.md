ADDED: getters for `KeyPathInfo`
REMOVED: `KeyPathExtractor`
ADDED: `KeyPathInfoExtractor`
BREAKING: `KeyMgr::insert` returns `Result<Option<K>>`
BREAKING: `KeyMgr::remove` returns `Result<Option<K>>`
